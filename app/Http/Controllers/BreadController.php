<?php

namespace App\Http\Controllers;

use App\Models\Bread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use function App\Helpers\errorResponse;
use function App\Helpers\successResponse;

class BreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {

        $breads = Bread::all();
        return successResponse($breads);
        // return successResponse("eads");
        // return $breads;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {

        try {
            $bread = Bread::findOrFail($id);
            return successResponse($bread);
        } catch (ModelNotFoundException $e) {
            return errorResponse("no data for id given", 404);
        }

        // $bread = Bread::find($id);
        //     if (!$bread) {
        //         return response('messag no data with id given');
        //     }
        //     return  $bread;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $bread = Bread::create([
            'name' => $request->name
        ]);
        return $bread;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bread = Bread::findOrFail($id);
        // $bread = Bread::find($id);
        if (!$bread) {
            return "no data";
        }
        $bread->fill($request->all());
        $bread->save();
        return $bread;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // $bread = Bread::findOrFail($id);
        $bread = Bread::find($id);
        if (!$bread) {
            return "no data";
        }
        $bread->delete();
        return response(['message' => 'delete succussefully']);
    }
}
