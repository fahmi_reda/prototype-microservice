<?php

namespace App\Http\Controllers;

require_once __DIR__ . '/../../../vendor/autoload.php';
$app = require_once __DIR__ . '/../../../bootstrap/app.php';
$app->boot();



// use App\Http\Controllers\BreadController;
use App\Models\Bread;
use Illuminate\Http\Request;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RpcReceiver
{
    /* ... SOME OTHER CODE HERE... */

    /**
     * Listens for incoming messages
     */



    public function listen()
    {
        $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest', '/');


        echo " [x] Awaiting RPC requests\n";

        $channel = $connection->channel();

        $channel->queue_declare(
            'rpc_queue',    #queue 
            false,          #passive
            false,          #durable
            false,          #exclusive
            false           #autodelete
        );

        $channel->basic_qos(
            null,   #prefetch size
            1,      #prefetch count
            null    #global
        );

        $channel->basic_consume(
            'rpc_queue',                #queue
            '',                         #consumer tag
            false,                      #no local
            false,                      #no ack
            false,                      #exclusive
            false,                      #no wait
            array($this, 'callback')    #callback
        );

        while (count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }

    /**
     * Executes when a message is received.
     *
     * @param AMQPMessage $req
     */
    public function callback(AMQPMessage $req)
    {
        // $id = null;
        // $data = null;
        //message from client
        $receivebody = json_decode($req->body);
        $type = $receivebody->type;
        // if ($receivebody->id) {
        //     $id = $receivebody->id;
        // }
        // if ($receivebody->data) {
        //     $data = $receivebody->data;
        // }


        //traitement de message 

        // $breads = Bread::all();
        $bread = new BreadController();
        switch ($type) {
            case 'list':
                $body = json_encode($bread->list());
                // $body = $bread->list();
                // dd(gettype($body));
                break;
            case 'read':


                $body = json_encode($bread->read($receivebody->id));
                // dd($body);
                break;
            case 'create':

                $request = new Request((array) $receivebody->data);

                $body = $bread->create($request);
                break;

            case 'update':

                $request = new Request((array) $receivebody->data);

                $body = $bread->update($request, $receivebody->id);
                break;
            case 'delete':


                $body = $bread->delete($receivebody->id);
                break;

            default:
                # code...
                break;
        }
        // if ($receivebody->type == 'list') { }
        // $body = $data;
        // $body = "chaine";
        // $body = json(['message' => 'success']);

        // $body = json_encode(array('message' => "no data with id given"));
        $msg = new AMQPMessage(
            $body,
            array('correlation_id' => $req->get('correlation_id'))
        );

        $req->delivery_info['channel']->basic_publish(
            $msg,
            '',
            $req->get('reply_to')
        );
        $req->ack();
    }
}

$server = new RpcReceiver();
$server->listen();
