<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('microservice/list', 'BreadController@list');
$router->get('microservice/read/{id}', 'BreadController@read');
$router->post('microservice/create', 'BreadController@create');
$router->put('microservice/update/{id}', 'BreadController@update');
$router->patch('microservice/update/{id}', 'BreadController@update');
$router->delete('microservice/delete/{id}', 'BreadController@delete');
$router->get('/', function () use ($router) {
    return $router->app->version();
});
