<?php

namespace App\Helpers;

use Illuminate\Http\Response;

function getText($text)
{
    return $text;
}

function successResponse($data, $code = Response::HTTP_OK)
{
    return response()->json(['data' => $data, 'code' => $code], $code);
}

/**
 * Build error response */

function errorResponse($message, $code)
{
    return response()->json(['error' => $message, 'code' => $code], $code);
}
